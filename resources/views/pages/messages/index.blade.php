@extends('index')

@section('title', 'Backend: Гостевая книга')
@section('pagetitle', 'Гостевая книга')

@section('content')

    <div class="alert alert-primary" role="alert">
        <h4 class="alert-heading">Всего сообщений: {{ $count }}</h4>
    </div>

	@include('input.form')

	<div class="row clearfix">
		<div class="col-md-12 column">


			@include('pages.messages.items')

			{{ $message->render() }}

		</div>

@stop
