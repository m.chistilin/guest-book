	@if(!$message->isEmpty())
		@foreach ($message as $message)
	<div class="well short_mult"> <!--onclick="window.location.href='/message/{!! $message->id !!}/'"!-->
				<div class="text">
					<h3>
					#{!! $message->id !!}
						<a href="/message/{!! $message->id !!}/"> {{ $message->name }}</a> 
					</h3>
					<h4 class="snum"><small>{!! $message->created_at !!}</small></h4>
					<p>{!! $message->message !!}</p>
				</div>
					@if(Auth::check() && Auth::getUser()->admin == 2)
					<div align="right">
						<button type="button" class="btn btn-info" onclick="window.location.href='/message/{!! $message->id !!}/edit'">Редактировать</button>					
						<button type="button" class="btn btn-danger" onclick="window.location.href='/message/{!! $message->id !!}/delete'" >Удалить</button>
					</div>
					@endif
			</div>
		@endforeach
	@else
			Сообщений нет
	@endif