@extends('index')

@section('title','Редактирование записи #'.$message->id.'')
@section('pagetitle', 'Редактирование записи книги #'.$message->id.'')


@section('content')

@if(count($errors) > 0)
	@foreach($errors->all() as $error)
		<li>{{$error}}</li>
	@endforeach
@endif


<div class="well short_mult"> <!--onclick="window.location.href='#'"!-->
	<div class="text">
		<h3>
			#{{ $message->id }}
			@unless (empty($message->email))
				<a href="mailto:{{ $message->email }}"> {{ $message->name }}</a>

			@else
				{{ $message->name }}
			@endunless
		</h3>
		<h4 class="snum"><small>{{ $message->created_at }}</small></h4>
			<p>{{ $message->message }}</p>
	</div>
</div>
<form method="post" action="{{route('bookupdate',['id' => $message->id])}}">
	<div class="row">
        <div class="form-group col-sm-6">
            <label for="name" class="h4">Имя</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $message->name }}" required>
            </div>
            <div class="form-group col-sm-6">
                <label for="email" class="h4">E-mail</label>
                <input type="email" class="form-control" name="email" "id="email" value="{{ $message->email }}" required>
            </div>


        <div class="form-group">
            <label for="message" class="h4 ">Сообщение</label>
            <textarea id="message" name="message" class="form-control" rows="5" value="{{ $message->message }}" required>{{ $message->message }}</textarea>
        </div>
    <button type="submit" id="form-submit" class="btn btn-lg btn-info btn-block">Сохранить</button>
		{{ csrf_field() }}
	</div>
</form>
@stop
