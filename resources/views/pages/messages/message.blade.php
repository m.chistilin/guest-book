@extends('index')

@section('title','Backend: Гостевая книга просмотр записи #'.$message->id.'')
@section('pagetitle', 'Просмотр записи #'.$message->id.'')

@section('content')


	<div class="well short_mult"> <!--onclick="window.location.href='#'"!-->
				<div class="text">
					<h3>
					#{{ $message->id }}
					@unless (empty($message->email))
						<a href="mailto:{{ $message->email }}"> {{ $message->name }}</a>
					@else
						 {{ $message->name }}
					@endunless
					</h3>
					<h4 class="snum"><small>{{ $message->created_at }}</small></h4>
					<p>{{ $message->message }}</p>
				</div>
			</div>
			@if(Auth::check() && Auth::getUser()->admin == 2)
				<div align="right">
					<button type="button" class="btn btn-info" onclick="window.location.href='/message/{{ $message->id }}/edit'">Редактировать</button>
					<button type="button" class="btn btn-danger" onclick="window.location.href='/message/{{ $message->id }}/delete'" >Удалить</button>
				</div>
			@endif


@stop
