<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Guest-Book backend</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</head>

<body>
<div class="container-fluid">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h1 class="logo">NOVA  <span style="color:red;">/</span> TEST <small>Гостевая книга</small></h1>
			<hr/>
		</div>
	</div>
	<div align="center">
	        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (!Auth::check())
						<button type="button" class="btn btn-primary btn-sm" onclick="document.location='{{ url('/login') }}'">Авторизация</button>
						<button type="button" class="btn btn-primary btn-sm" onclick="document.location='{{ url('/register') }}'">Регистрация</button>
                    @else
						<button type="button" class="btn btn-primary btn-sm" href="{{ url('/logout') }}"
												onclick=" event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Выйти из аккаунта</button>
										<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
						
					@endif
                </div>
            @endif


        </div></div>
	<div class="row">
	@if (Route::has('login'))
		@if (!Auth::check())
            <div class="form-group col-sm-6">
                <label for="name" class="h4">Имя</label>
                <input type="text" class="form-control" id="name" placeholder="Введите имя" required>
            </div>
            <div class="form-group col-sm-6">
                <label for="email" class="h4">E-mail</label>
                <input type="email" class="form-control" id="email" placeholder="Введите E-mail" required>
            </div>
        
		@endif
	@endif	
	</div>
		@if (Route::has('login'))
		@if (Auth::check())
		<p> Уважаемый, {{ Auth::user()->name }} пожалуйста оставьте свое сообщение! </p>
	
			@endif
	@endif	
        <div class="form-group">
            <label for="message" class="h4 ">Сообщение</label>
            <textarea id="message" class="form-control" rows="5" placeholder="Введите сообщение" required></textarea>
        </div>
        <button type="submit" id="form-submit" class="btn btn-lg btn-success btn-block">Отправить</button>
		<br />

	<div class="row clearfix">
		<div class="col-md-12 column">
			
			
			
			<div class="well short_mult"> <!--onclick="window.location.href='#'"!-->
				<div class="text">
					<h3>Ivan </h3>
					<h4 class="snum"><small>22-01-11 12:11</small></h4>
					<p>Здесь текст текст						</p>
				</div>
			</div>
			
			
			
			<ul class="pagination">
				<li>
					<a href="#">Назад</a>
				</li>
				<li>
					<a href="#">1</a>
				</li>
				<li>
					<a href="#">2</a>
				</li>
				<li>
					<a href="#">3</a>
				</li>
				<li>
					<a href="#">4</a>
				</li>
				<li>
					<a href="#">5</a>
				</li>
				<li>
					<a href="#">Вперед</a>
				</li>
			</ul>
		</div>

</div>
</body>
</html>
