@if(count($errors) > 0)
	@foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {!!  $error !!}
        </div>
	@endforeach
@endif
	<form method="post" action = "{{route('bookcreate')}}">
	<div class="row">
	@if (Route::has('login'))
		@if (!Auth::check())
            <div class="form-group col-sm-6">
                <label for="name" class="h4">Имя</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Введите имя" required>
            </div>
            <div class="form-group col-sm-6">
                <label for="email" class="h4">E-mail</label>
                <input type="email" class="form-control" name="email" "id="email" placeholder="Введите E-mail" required>
            </div>

		@endif
	@endif
	</div>
		@if (Route::has('login'))
		@if (Auth::check())
		<p> Уважаемый, {{ Auth::user()->name }} пожалуйста оставьте свое сообщение! </p>
			<input type="hidden" id="name" name="name" value="{{ Auth::user()->name }}">
			<input type="hidden" id="email" name="email" value="{{ Auth::user()->email }}">
			@endif
	@endif
        <div class="form-group">
            <label for="message" class="h4 ">Сообщение</label>
            <textarea id="message" name="message" class="form-control" rows="5" placeholder="Введите сообщение" required></textarea>
        </div>
        <button type="submit" id="form-submit" class="btn btn-lg btn-success btn-block">Отправить</button>
		{{ csrf_field() }}
		</form>
		<br />
