<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>@yield('title')</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->

	<link href="{{ url('/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ url('/css/style.css') }}" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ url('img/apple-touch-icon-144-precomposed.png') }}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ url('img/apple-touch-icon-114-precomposed.png') }}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ url('img/apple-touch-icon-72-precomposed.png') }}">
  <link rel="apple-touch-icon-precomposed" href="{{ url('img/apple-touch-icon-57-precomposed.png') }}">
  <link rel="shortcut icon" href="{{ url('img/favicon.png') }}">

	<script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/scripts.js') }}"></script>
</head>

<body>
<div class="container-fluid">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h1 class="logo"><a href="/">NOVA  <span style="color:red;">/</span> TEST</a> <small>@yield('pagetitle')</small></h1>
			<hr/>
		</div>
	</div>
	<div align="center">
	        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (!Auth::check())
						<button type="button" class="btn btn-primary btn-sm" onclick="document.location='{{ url('/login') }}'">Авторизация</button>
						<button type="button" class="btn btn-primary btn-sm" onclick="document.location='{{ url('/register') }}'">Регистрация</button>
                    @else
						<button type="button" class="btn btn-primary btn-sm" href="{{ url('/logout') }}"
												onclick=" event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Выйти из аккаунта</button>
										<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

					@endif
                </div>
            @endif


        </div></div>

	<div class="row clearfix">
		@yield('content')
		</div>

</div>

</body>
</html>
