<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'MessageHomeController@index'])->name('HomePage');

Route::get('message/{id}/edit', ['uses' => 'MessageHomeController@edit'])->where(['id' => '[0-9]+'])->name('MessageEdit');
Route::get('message/{id}', ['uses' => 'MessageHomeController@message'])->where(['id' => '[0-9]+'])->name('MessageID');
Route::post('message/{id}/edit', 'MessageHomeController@update_book')->where(['id' => '[0-9]+'])->name('bookupdate');
Route::get('message/{id}/delete', 'MessageHomeController@delete_book')->name('deltebook');
Route::post('/', ['uses' => 'MessageHomeController@create'])->name('bookcreate');

Auth::routes();

Route::get('/home', 'HomeController@index');
