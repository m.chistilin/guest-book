<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestCreateMessage;
use App\Models\Message;
use Auth;
use Illuminate\Http\Request;

class MessageHomeController extends Controller
{
    public function index()
    {
        $data = [
            'message' => Message::latest()->paginate(5),
            'count' => Message::count()
        ];
        return view('pages.messages.index', $data);
    }
    public function edit($id)
    {
        if(!Auth::check() || !Auth::getUser()->admin == 2)
            return redirect()->route('MessageID', $id);
        if(is_null(Message::find($id)))
            return redirect()->route('HomePage');
        /*$message = Message::find($id);
        return view('pages.messages.edit',compact($message));*/
        return view('pages.messages.edit', ['message' =>Message::find($id)]);
    }
    public function message($id)
    {
        return view('pages.messages.message',
            ['message' => Message::find($id)]
        );
    }
    public function create(RequestCreateMessage $request)
    {
        $data = $request->all();
        $words = array('пидор','хуй','гандон','плохое слово');
        foreach($words as $word)
        {
            if(stristr($data["message"], $word))
            {
                return redirect()->route('HomePage');
            }
        }
        Message::create($data);
        return redirect()->route('HomePage');
    }
    public function update_book($id, Request $request)
    {
        if(!Auth::check() || !Auth::getUser()->admin == 2)
            return redirect()->route('MessageID', $id);
        $data = $request->all();
        $message = Message::find($id);
        $message->update($data);
        $message->save();
        return redirect()->route('MessageEdit', $id);
    }
    public function delete_book($id, Request $request)
    {
        if(!Auth::check() || !Auth::getUser()->admin == 2)
            return redirect()->route('HomePage');
        if(is_null(Message::find($id)))
            return redirect()->route('HomePage');
        $message = Message::find($id);
        $message->delete();
        return redirect()->route('HomePage');
    }
}
