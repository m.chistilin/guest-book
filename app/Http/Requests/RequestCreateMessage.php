<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class RequestCreateMessage extends FormRequest
{
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|max:255',
                'message' => 'required|max:1024|alpha_dash',
                'email' => 'required|max:255|email'
            ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Необходимо заполнить поле Имя',
            'message.alpha_dash'  => 'Поле должно содержать только алфавитные символы, цифры, знаки подчёркивания (_) и дефисы (-)',
            'message.max'  => 'Максимальное количество символов равно 1024',
            'message.required'  => 'Необходимо заполнить поле Сообщение',
            'email.required'  => 'Необходимо заполнить поле E-mail',
            'email.max'  => 'Максимальная длина строки "Email" 255 символов',
            'email.email'  => 'Ваша почта должна быть в виде test@mail.ru'
        ];
    }
}
